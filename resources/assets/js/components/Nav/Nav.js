import React, {Component} from 'react'
import { NavLink, withRouter } from 'react-router-dom'
import Search from './Search';

//@withRouter
class Nav extends Component {
    constructor(props) {
        super(props)
        this.onSearch = this.onSearch.bind(this)
        this.updateTerm = this.updateTerm.bind(this)
        this.state = {
            searchTerm: ''
        }
    }

    onSearch(e) {
        e.preventDefault();
        //const { history: { pushState } } = this.props;
        this.props.history.push('/products?title='+this.state.searchTerm)
    }

    updateTerm(e) {
        this.setState({
            searchTerm: e.target.value
        })
    }

    render() {
        return (
            <nav className="navbar navbar-inverse">
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <a className="navbar-brand" href="#">Brand</a>
                    </div>

                    <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul className="nav navbar-nav">
                            <li className="dropdown">
                                <NavLink exact activeClassName="active" to="/products" className="dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Products <span className="caret"></span></NavLink>
                                <ul className="dropdown-menu">
                                    <li><NavLink exact activeClassName="active" to="/products">List</NavLink></li>
                                    <li><NavLink exact activeClassName="active" to="/products/add">Add</NavLink></li>
                                    <li role="separator" className="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                    <li role="separator" className="divider"></li>
                                    <li><a href="#">One more separated link</a></li>
                                </ul>
                            </li>
                        </ul>
                        <Search updateTerm={this.updateTerm} searchTerm={this.state.searchTerm} onSearch={this.onSearch} />
                        <ul className="nav navbar-nav navbar-right">
                            <li><a href="#">Link</a></li>
                            <li className="dropdown">
                                <a href="#" className="dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-haspopup="true" aria-expanded="false">Dropdown <span
                                    className="caret"></span></a>
                                <ul className="dropdown-menu">
                                    <li><a href="#">Action</a></li>
                                    <li><a href="#">Another action</a></li>
                                    <li><a href="#">Something else here</a></li>
                                    <li role="separator" className="divider"></li>
                                    <li><a href="#">Separated link</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        )
    }

}

export default  withRouter(Nav)