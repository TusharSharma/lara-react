import React, {Component} from 'react'

const Search = ({onSearch, updateTerm, searchTerm}) =>
<form className="navbar-form navbar-left" onSubmit={onSearch} >
    <div className="form-group">
        <input type="text" className="form-control" placeholder="Search" defaultValue={searchTerm} onChange={updateTerm} />
    </div>
    <button type="submit" className="btn btn-default">Submit</button>
</form>

export default Search;