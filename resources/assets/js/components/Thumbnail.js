import React from 'react'

const Thumbnail = ({item}) =>
(item.has_thumb && <div className="crop thumbnail"><img src={item.thumb} /></div>) ||
(item.image && <div className="crop"><img src={item.image} /></div>)


export default Thumbnail;