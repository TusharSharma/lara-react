import React, {Component} from 'react'
import Loader from '../Loader'
import { Link } from 'react-router-dom'
import Thumbnail from '../Thumbnail'
import Sortable from '../Sortable'
import Pagination from '../Pagination'
import {toast} from 'react-toastify'
import queryString from 'query-string'
import * as config from '../../constants'

/**
 * @todo Update URL instead of explicitly calling _initializeComponent
 */
export default class extends Component {
    constructor(props) {
        super(props)

        const qs = queryString.parse(props.location.search);

        this.state = {
            products: [],
            loaded: false,
            pagination: {
                current: 1,
                prev: null,
                next: null
            },
            sortOptions : {
                field: qs.order || 'id',
                direction: qs.direction || 'asc'
            }
        }

        this._updateUrl = this._updateUrl.bind(this)
        this._initializeComponent = this._initializeComponent.bind(this)
        this.deleteProduct = this.deleteProduct.bind(this)
        this.changePage = this.changePage.bind(this)
        this.sort = this.sort.bind(this)

    }

    _updateUrl() {

        const qs = queryString.parse(this.props.location.search);
        qs.page = this.state.pagination.current;

        //Perhaps trying to access state before new one has been reflected
        //if(this.state.sortOptions.direction !== 'asc' || this.state.sortOptions.field !== 'id') {
        // if(!(this.state.sortOptions.direction === 'asc' && this.state.sortOptions.field === 'id')) {
        //     console.log('Setting Order');


            qs.order = this.state.sortOptions.field;
            qs.direction = this.state.sortOptions.direction;


        // } else {
        //     console.log('Deleting Order');
        //     delete(qs.order);
        //     delete(qs.direction);
        // }

        this.props.history.push(`/products?${queryString.stringify(qs)}`)


    }

    _initializeComponent(props) {
        //?${queryString.stringify(qs)}
        axios
        .get(`${config.BASE_URL}/products${props.location.search}`)
        .then(response => {
            this.setState({
                products: response.data.data,
                loaded: true,
                pagination: {
                    current: response.data.current_page,
                    next: response.data.next_page_url,
                    prev: response.data.prev_page_url
                }
            })
        })
        .catch(error => toast.error(
            (error.response && error.response.data && error.response.data.message) || error.message
        ))
    }

    sort(field, direction) {
        const sortOptions = {field, direction};
        this.setState({loaded:false, sortOptions}, this._updateUrl);
        //this._initializeComponent(this.props);
        
    }

    changePage(page, e) {
        e.preventDefault();
        if(page === null) {
            return;
        }
        const pagination = this.state.pagination;
        pagination.current = page;
        this.setState({
            loaded: false,
            pagination: pagination
        });
        //this._initializeComponent(this.props);
        this._updateUrl();
    }

    //FIXME: Pagination probably won't work well after deletion of products
    deleteProduct(product) {
        axios
        .delete(`${config.BASE_URL}/products/${product.id}`)
        .then(response => {
            if(response.data.success === true) {
                toast(response.data.message)
                this.setState(previousState => {
                    const products = previousState.products
                    const index = previousState.products.indexOf(product)
                    products.splice(index, 1)
                    return {products:products}
                })
            } else {
                toast.error(response.data.message)
            }
        })
        .catch(error => toast.error(
            (error.response && error.response.data && error.response.data.message) || error.message
        ))
    }

    componentWillReceiveProps(nextProps) {

        //TODO: match after parsing to allow params in any order
        if(this.props.location.search !== nextProps.location.search) {
            this.setState({loaded: false})
            this._initializeComponent(nextProps)
        }
    }

    componentDidMount() {
        const props = this.props
        this._initializeComponent(props)
    }

    render() {
        const loaded = this.state.loaded
        const loader = <tr><td colSpan="6"><Loader/></td></tr>
        const products = this.state.products.length > 0 ? 
        this.state.products.map(
            (item, i) => 
            <tr key={i}>
                <td>{item.id}</td>
                <td>{item.title}</td>
                <td>{item.price}</td>
                <td>{item.description && item.description.substr(0, 100)}</td>
                <td><Thumbnail item={item} /></td>
                <td>
                    <button title="Delete Product" onClick={this.deleteProduct.bind(this, item)} className="btn btn-sm btn-danger"><i className="material-icons">&#xE872;</i></button>
                    <Link title="Edit Product" to={{
                        pathname: "/products/" + item.id + "/edit",
                        product: item
                    }} className="btn btn-sm btn-warning"><i className="material-icons">&#xE3C9;</i></Link>
                </td>  
            </tr>
        )
        : <tr><td className="text-center" colSpan="6">No Products Found</td></tr>

        return(
            <div>
            <table className="table">
                <thead>
                    <tr>
                        <Sortable sortField='id' current={this.state.sortOptions} onClick={this.sort}>ID</Sortable>
                        <Sortable sortField='title' current={this.state.sortOptions} onClick={this.sort}>Title</Sortable>
                        <Sortable sortField='price' current={this.state.sortOptions} onClick={this.sort}>Price</Sortable>
                        <th>Description</th>
                        <th>Image</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {!loaded ? loader : products}
                </tbody>
            </table>
            <Pagination data={this.state.pagination} onPageChange={this.changePage} />
            </div>
        )
    }
}