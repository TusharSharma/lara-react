import React, {Component} from 'react'
import Form from './Form'
import {toast} from 'react-toastify'
import * as config from '../../constants'

export default class extends Component {
    constructor(props) {
        super(props)

        this.state = {
            form: {}
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        
    }

    handleSubmit(event) {
        event.preventDefault()
    
        const formData = new FormData(event.target)

        axios.post(`${config.BASE_URL}/products`, formData)
        .then(response => {
            if(response.data.success === true) {
                toast(response.data.message)
                this.props.history.push('/products')
            } else {
                toast.error(response.data.message)
            }
        })
        .catch(error => toast.error((error.response && error.response.data && error.response.data.message) || error.message))
    }

    render() {
        return (
            <div>
                <Form onSubmit={this.handleSubmit} />
            </div>
        )
    }
}