import React from 'react'
import Thumbnail from '../Thumbnail';

const Form = ({product, onSubmit, onChange}) =>
<form onSubmit={onSubmit} method="post" encType="multipart/form-data">
    <div className="form-group">
        <label htmlFor="title">Title:</label>
        <input type="text" className="form-control" id="title" name="title" defaultValue={product && product.title} />
    </div>
    <div className="form-group">
        <label htmlFor="price">Price:</label>
        <input type="text" className="form-control" id="price" name="price" defaultValue={product && product.price} />
    </div>
    <div className="form-group">
        <label htmlFor="description">Description:</label>
        <textarea type="text" className="form-control" id="description" name="description" defaultValue={product && product.description} />
    </div>
    <div className="form-group">
        <label htmlFor="image">Image:</label>
        <input type="file" id="image" name="image" className="form-control" />
    </div>
    <div className="form-group">
        {product && <Thumbnail item={product} />}
    </div>
    <button type="submit" className="btn btn-default">Submit</button>
</form>

export default Form;