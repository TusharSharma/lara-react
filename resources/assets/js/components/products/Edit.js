import React, {Component} from 'react'
import Form from './Form'
import Loader from '../Loader';
import {toast} from 'react-toastify';
import * as config from '../../constants';

export default class extends Component {
    constructor(props) {
        super(props)

        this.state = {
            form: {},
            product: props.location.product || null
        }

        this.handleSubmit = this.handleSubmit.bind(this)
        this._takeToHome = this._takeToHome.bind(this)
    }

    _takeToHome() {
        this.props.history.push('/products')
    }

    componentDidMount() {
        //load product if it was not passed as a prop
        if(this.state.product === null) {
            axios.get(`${config.BASE_URL}/products/${this.props.match.params.id}`)
            .then(response => {
                if(response.data !== null) {
                    this.setState({
                        product: response.data
                    })
                } else {
                    //TODO: take back after displaying error
                    toast.error(response.data.message, {
                        onClose: this._takeToHome
                    })
                }
            })
            .catch(error => toast.error(
                (error.response && error.response.data && error.response.data.message) || error.message, {
                onClose: this._takeToHome
            }))
        }

    }

    handleSubmit(event) {
        event.preventDefault();
    
        const formData = new FormData(event.target);

        formData.append('_method','PUT'); //queryString.stringify(formData) doesn't appear to be working with axios.put

        axios.post(`${config.BASE_URL}/products/${this.state.product.id}`, formData)
        .then(response => {
            if(response.data.success === true) {
                toast(response.data.message)
                this.setState({
                    product: response.data.product
                })
            } else {
                toast.error(response.data.message)
            }
        })
        .catch(error => toast.error((error.response && error.response.data && error.response.data.message) || error.message))
    }

    render() {
        return (
            <div>
                {!this.state.product ? <Loader/> : <Form onSubmit={this.handleSubmit} product={this.state.product} />}
            </div>
        )
    }
}