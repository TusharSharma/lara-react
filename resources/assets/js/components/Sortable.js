import React, {Component} from 'react'

export default class Sortable extends Component {

    render() {

        const {sortField} = this.props;

        const {field, direction} = this.props.current;

        const arrowDown = <i className="material-icons">&#xE313;</i>;   //Descending
        const arrowUp = <i className="material-icons">&#xE316;</i>;     //Ascending

        return (
            <th onClick={this.props.onClick.bind(null, sortField, field === sortField && direction!=='asc'?'asc':'desc')}>{this.props.children} {sortField === field && direction === 'desc' ? arrowDown : arrowUp}</th>
        )
    }
}