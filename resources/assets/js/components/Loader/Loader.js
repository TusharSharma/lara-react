import React from 'react'

const Loader = () => <div className="loader"><div className="loading"></div></div>
export default Loader;