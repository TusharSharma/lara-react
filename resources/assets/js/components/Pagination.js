import React, {Component} from 'react'

// const Pagination = (props) => 
// <ul className="pager">
//   <li className="previous"><a href="#" className="material-icons">&#xE314;</a></li>
//   <li className="next"><a href="#" className="material-icons">&#xE315;</a></li>
// </ul>

class Pagination extends Component {
  constructor(props) {
    super(props)

  }

  componentWillMount() {
    this._initializeComponent(this.props);
  }

  componentWillReceiveProps(nextProps) {
    this._initializeComponent(nextProps);
  }

  _initializeComponent(props) {
    const {current, next, prev} = props.data;
    this.setState({
      prevPage: prev !== null && current > 1 ? current - 1 : null,
      nextPage: next !== null ? current + 1 : null
    })
  }

  render() {
    return (
      <ul className="pager">
        <li className={['previous', this.state.prevPage === null ? 'disabled' : ''].join(' ')}><a onClick={this.props.onPageChange.bind(null, this.state.prevPage)} href="#" className="material-icons">&#xE314;</a></li>
        <li className={['next',this.state.nextPage === null ? 'disabled' : ''].join(' ')}><a onClick={this.props.onPageChange.bind(null, this.state.nextPage)} href="#" className="material-icons">&#xE315;</a></li>
      </ul>
    )
  }

}

export default Pagination;