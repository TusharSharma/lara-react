import React, {Component} from 'react'
import Nav from '../components/Nav';
import {ToastContainer} from 'react-toastify';

import ProductsList from '../components/products/List';
import ProductsAdd from '../components/products/Create';
import ProductsEdit from '../components/products/Edit';

import {
    BrowserRouter as Router,
    Route
  } from 'react-router-dom'


export default class Master extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Router>
            <div className="container">
                <Nav />
                <Route exact path="/products" component={ProductsList}/>
                <Route path="/products/add" component={ProductsAdd}/>
                <Route path="/products/:id/edit" component={ProductsEdit}/>
                <ToastContainer />
            </div>
            </Router>
        )
    }
}