import './bootstrap';
import React, {Component} from 'react'
import {render} from 'react-dom'
import Master from './layouts/Master';

if(document.getElementById('root')){
    render(<Master/>, document.getElementById('root'));
}

