<?php

namespace App\Http\Controllers;

use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Exception\NotSupportedException;
use Intervention\Image\ImageManager;

/**
 * Class Products
 * @package App\Http\Controllers
 * @todo add validations
 * @todo dry asset paths
 */
class Products extends Controller
{
    private $pathImages;

    private $pathThumbs;

    private $perPage = 2;

    public function __construct()
    {
        $this->pathImages = public_path('storage' . DIRECTORY_SEPARATOR . 'images') . DIRECTORY_SEPARATOR;
        $this->pathThumbs = $this->pathImages . 'thumb' . DIRECTORY_SEPARATOR;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $orderField = Input::get('order') ?? 'id';
        $orderDirection = Input::get('direction') ?? 'asc';

        $searchTerm = Input::get('title');
        if (!empty($searchTerm)) {
            $products = Product::orderBy($orderField, $orderDirection)
                ->where('title', 'like', "%{$searchTerm}%")
                ->simplePaginate($this->perPage);
        } else {
            $products = Product::orderBy($orderField, $orderDirection)->simplePaginate($this->perPage);
        }

        return response($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $product = new Product();
        $product->title = $request->post('title');
        $product->price = $request->post('price');
        $product->description = $request->post('description');


        $image = $request->file('image');

        if ($image !== null) {
            $imageName = md5(mt_rand() . $image->getClientOriginalName()) . '.' . $image->getClientOriginalExtension();

            $thumb = null;
            try {
                $manager = new ImageManager(['driver' => 'gd']);
                $thumb = $manager->make($image);
            } catch (NotSupportedException $exception) {
                try {
                    $manager = new ImageManager(['driver' => 'imagick']);
                    $thumb = $manager->make($image);
                } catch (NotSupportedException $exception) {
                    //Skipping Thumbnail Creation
                }
            }

            if ($thumb !== null) {
                $thumb->fit(200)->save($this->pathThumbs . $imageName);
                $product->has_thumb = true;
            }


            $image->move($this->pathImages, $imageName);
            $product->image = $imageName;
        }

        $saved = false;
        try {
            $saved = $product->save();
        } catch (\Exception $exception) {
            if(!empty($imageName)) {
                $this->deleteImage($imageName);
            }
        }

        if ($saved) {
            return response([
                'success' => true,
                'product' => $product,
                'message' => 'Product Created',
            ]);
        } else {
            if (!empty($imageName)) {
                $this->deleteImage($imageName);
            }
            return response([
                'success' => false,
                'message' => 'Product Creation Failed'
            ]);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $product = Product::find($id);
        if($product === null) {
            return response([
                'message' => 'Product Not Found'
            ], 404);
        }

        return response($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->title = $request->input('title');
        $product->price = $request->input('price');
        $product->description = $request->input('description');

        $image = $request->file('image');
        $filesToDelete = [];

        if ($image !== null) {

            //Files to be removed if new image is uploaded
            if (!empty($product->getOriginal('image'))) {
                $filesToDelete[] = 'images' . DIRECTORY_SEPARATOR . $product->getOriginal('image');
            }
            if ($product->has_thumb) {
                $filesToDelete[] = 'images' . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $product->getOriginal('image');
            }


            $imageName = md5(mt_rand() . $image->getClientOriginalName()) . '.' . $image->getClientOriginalExtension();

            #region Thumbnail Creation
            $thumb = null;
            $product->has_thumb = false;
            try {
                $manager = new ImageManager(['driver' => 'gd']);
                $thumb = $manager->make($image);
            } catch (NotSupportedException $exception) {
                try {
                    $manager = new ImageManager(['driver' => 'imagick']);
                    $thumb = $manager->make($image);
                } catch (NotSupportedException $exception) {
                    //Skipping Thumbnail Creation
                }
            }

            if ($thumb !== null) {
                $thumb->fit(200)->save($this->pathThumbs . $imageName);
                $product->has_thumb = true;
            }
            #endregion Thumbnail Creation

            $image->move($this->pathImages, $imageName);
            $product->image = $imageName;
        }

        $saved = false;
        try {
            $saved = $product->save();
        } catch (\Exception $exception) {
            if(!empty($imageName)) {
                $this->deleteImage($imageName);
            }
        }

        if ($saved) {
            Storage::disk('public')->delete($filesToDelete);
            return response([
                'success' => true,
                'product' => $product,
                'message' => 'Product Updated',
            ]);
        } else {
            if (!empty($imageName)) {
                $this->deleteImage($imageName);
            }
            return response([
                'success' => false,
                'message' => 'Product Update Failed',
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        $filesToDelete = [];
        if (!empty($product->getOriginal('image'))) {
            $filesToDelete[] = 'images' . DIRECTORY_SEPARATOR . $product->getOriginal('image');
        }
        if ($product->has_thumb) {
            $filesToDelete[] = 'images' . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $product->getOriginal('image');
        }

        Storage::disk('public')->delete($filesToDelete);

        $product->delete();

        return response([
            'success' => true,
            'message' => 'Product Deleted',
        ]);
    }

    /**
     * @param $imageName
     */
    private function deleteImage($imageName)
    {
        Storage::disk('public')->delete([
            'images' . DIRECTORY_SEPARATOR . $imageName,
            'images' . DIRECTORY_SEPARATOR . 'thumb' . DIRECTORY_SEPARATOR . $imageName,
        ]);
    }
}
