<?php

namespace App\Http\Middleware;

use Closure;

class HandleReactRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->ajax()) {
            return response()->make(view('home'));
        }

        return $next($request);
    }
}
