<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $appends = array('thumb');
    
    public function getImageAttribute($value)
    {
        return !empty($value) ? asset("storage/images/$value") : null;
    }

    public function getThumbAttribute()
    {
        return $this->hasThumb ? asset('storage/images/thumb/'.$this->getOriginal('image')) : null;
    }

    public function getHasThumbAttribute()
    {
        return $this->getOriginal('has_thumb') != 0;
    }
}
